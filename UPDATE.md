# 2022-03-15 提交代码统计

```
[Running] python -u "d:\code\java\csmall-pms-client\countline.py"
./babel.config.js---- 5
./public\index.html---- 17
./src\App.vue---- 237
./src\main.js---- 20
./src\common\StorageUtils.js---- 16
./src\components\PhotoAlbumItem.vue---- 77
./src\components\PhotoPicItem.vue---- 125
./src\request\http.js---- 94
./src\request\api\album.js---- 54
./src\request\api\attribute.js---- 85
./src\request\api\attrTemplate.js---- 71
./src\request\api\base.js---- 8
./src\request\api\brand.js---- 58
./src\request\api\brandCategory.js---- 27
./src\request\api\category.js---- 114
./src\request\api\index.js---- 25
./src\request\api\pictures.js---- 43
./src\request\api\sku.js---- 50
./src\request\api\spu.js---- 39
./src\request\api\user.js---- 0
./src\router\index.js---- 222
./src\store\index.js---- 13
./src\views\Home.vue---- 5
./src\views\auth\PermissionAssignment.vue---- 120
./src\views\auth\PermissionList.vue---- 185
./src\views\auth\RoleList.vue---- 182
./src\views\auth\UserList.vue---- 272
total lines: 2164
Done! Cost Time: 0.38 second
```



# 2022-04-15 代码统计

与上次代码提交相比：

```
[Running] python -u "d:\code\java\csmall-pms-client\countline.py"
./babel.config.js---- 5
./public\index.html---- 17
./src\App.vue---- 5
./src\main.js---- 20
./src\common\StorageUtils.js---- 18
./src\components\PhotoAlbumItem.vue---- 77
./src\components\PhotoPicItem.vue---- 129
./src\request\http.js---- 115
./src\request\api\album.js---- 55
./src\request\api\attribute.js---- 86
./src\request\api\attrTemplate.js---- 72
./src\request\api\base.js---- 13
./src\request\api\brand.js---- 103
./src\request\api\brandCategory.js---- 30
./src\request\api\brandCategoryRelations.js---- 30
./src\request\api\category.js---- 119
./src\request\api\index.js---- 27
./src\request\api\pictures.js---- 44
./src\request\api\sku.js---- 53
./src\request\api\spu.js---- 56
./src\request\api\user.js---- 20
./src\router\index.js---- 234
./src\store\index.js---- 35
./src\views\Home.vue---- 248
./src\views\auth\PermissionAssignment.vue---- 120
./src\views\auth\PermissionList.vue---- 185
./src\views\auth\RoleList.vue---- 182
./src\views\auth\UserList.vue---- 272
./src\views\index\Index.vue---- 5
total lines: 2375
Done! Cost Time: 0.38 second
```

新增代码行数：211行。

重构修改代码行数：232行。

新增提交：views/product文件夹下所有代码。

```
[Running] python -u "d:\code\java\csmall-pms-client\countline.py"
./src\views\product\album\AlbumManage.vue---- 142
./src\views\product\album\PictureManage.vue---- 172
./src\views\product\attribute\AttributeTemplateManage.vue---- 347
./src\views\product\attribute\NoSalesAttributeAdd.vue---- 161
./src\views\product\attribute\NoSalesAttributeManage.vue---- 217
./src\views\product\attribute\NoSalesAttributeUpdate.vue---- 174
./src\views\product\attribute\SalesAttributeAdd.vue---- 162
./src\views\product\attribute\SalesAttributeManage.vue---- 218
./src\views\product\attribute\SalesAttributeUpdate.vue---- 175
./src\views\product\brand\BrandAdd.vue---- 208
./src\views\product\brand\BrandManage.vue---- 394
./src\views\product\brand\BrandUpdate.vue---- 178
./src\views\product\category\CategoryAdd.vue---- 201
./src\views\product\category\CategoryManage.vue---- 535
./src\views\product\category\CategoryUpdate.vue---- 209
./src\views\product\sku\SkuManage.vue---- 520
./src\views\product\spu\SpuAddStep1.vue---- 126
./src\views\product\spu\SpuAddStep2.vue---- 332
./src\views\product\spu\SpuAddStep3.vue---- 307
./src\views\product\spu\SpuAddStep4.vue---- 97
./src\views\product\spu\SpuManage.vue---- 615
total lines: 5490
Done! Cost Time: 0.39 second

[Done] exited with code=0 in 0.464 seconds
```

新增代码行数：5490行。