import axios from 'axios'; // 引入axios
import store from '../store/index';
import { Loading } from 'element-ui'
import qs from 'qs'

// 创建axios实例
var instance = axios.create({timeout: 1000 * 12});
// 设置post请求头
instance.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded';
// 等待框Loading示例
var loadingInstance;

/**
 * 设置请求拦截器，功能如下：
 * 1. 每次请求弹出等待框
 * 2. 每次请求从vuex中寻找token，若找到token则设置请求消息头，携带token一同发送请求
 */
instance.interceptors.request.use(    
    config => {        
        // 弹出loading等待框
        loadingInstance = Loading.service({
            lock: true,
            text: '加载中请稍后...',
            spinner: 'el-icon-loading',
            background: 'rgba(0, 0, 0, 0.7)'
        });

        // 每次发送请求之前判断vuex中是否存在token        
        // 如果存在，则统一在http请求的header都加上token，这样后台根据token判断你的登录情况
        // 即使本地存在token，也有可能token是过期的，所以在响应拦截器中要对返回状态进行判断
        if(store.state.tokenInfo){ 
            const tokenInfo = store.state.tokenInfo
            if(tokenInfo){
                // 将token信息添加到请求header中
                let tokenHeader = tokenInfo.tokenHeader
                let tokenValue = tokenInfo.tokenValue
                config.headers[tokenHeader] = tokenValue
            }     
        }
        return config;    
    },    
    error => {        
        loadingInstance.close()  // 请求发送失败，关闭loading框
        return Promise.error(error)   
})

/**
 * 设置响应拦截器，功能如下：
 * 1. 关闭loading等待框。
 * 2. 处理响应继续向后传递
 */
instance.interceptors.response.use(    
    response => {
        loadingInstance.close() // 关闭loading框
           
        // 如果返回的状态码为200，说明接口请求成功，可以正常拿到数据     
        // 否则的话抛出错误
        if (response.status === 200) {            
            return Promise.resolve(response);        
        } else {            
            return Promise.reject(response);        
        }    
    },    
    // 服务器状态码不是200时，执行error
    error => {            
        loadingInstance.close() // 关闭loading框
    }    
);

const myAxios = {
    /**
     * 发送get请求
     * @param {string} url 请求路径
     * @param {Object} params 请求参数
     * @returns 
     */
     get(url, params){
        return instance({
            method: 'get',
            url: url,
            params: params
        })
    },

    /**
     * 发送post请求
     * @param {string} url 请求路径
     * @param {Object} params 请求参数
     * @returns 
     */
    post(url, params){
        return instance({
            method: 'post',
            url: url,
            data: qs.stringify(params)
        })
    }
}

export default myAxios;