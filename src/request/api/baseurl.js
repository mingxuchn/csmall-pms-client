/**
 * 接口域名的管理
 */
const base = {    
    BASEURL_DEV: {                                      // 测试环境下的请求域名前缀
        PMS_BASEURL: 'http://localhost:9010',           // 后台管理服务请求域名前缀
        SSO_BASEURL: 'http://localhost:10002',          // 用户管理服务请求域名前缀
        UPLOAD_BASEURL: 'http://localhost:9060',           // 上传服务请求域名前缀
    },       
    BASEURL_PRODUCT: {                                  // 生产环境下的请求域名前缀
        PMS_BASEURL: 'http://212.11.156.22',    
    }
}

export default base.BASEURL_DEV;