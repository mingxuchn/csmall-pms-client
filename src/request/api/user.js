/** user业务模块接口整合 */
import base from './baseurl'           // 导入接口域名列表
import myAxios from '../MyAxios'         // 导入http中创建的axios实例


let SSO_BASEURL = base.SSO_BASEURL

const userApi = {   
    
    /**
     * 单点登录认证 登录
     * @param {Object} params 参数对象
     *  例如：{usename: 'zs', password: '1234'}
     * @returns {Promise}
     */
     login(params) {
        return myAxios.post(
            `${SSO_BASEURL}/admin/sso/login`, 
            params
        )
    },

}

export default userApi