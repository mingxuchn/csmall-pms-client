/** category业务模块接口整合 */
import base from './baseurl'           // 导入接口域名列表
import myAxios from '../MyAxios'         // 导入http中创建的axios实例

let PMS_BASEURL = base.PMS_BASEURL;

const categoryApi = {
  /**
   * 根据类别ID，查询类别详情
   * @param {Object} params 封装类别ID信息。例如： {id: 1}
   */
  queryById(id) {
    return myAxios.get(`${PMS_BASEURL}/pms/categories/${id}`);
  },

  /**
   * 根据品牌id查询类别列表接口
   * @param {Object} params 查询品牌所需参数对象，
   *        例如：{brandId:品牌ID}
   * @returns {Promise}
   */
  queryByBrandId(params) {
    return myAxios.get(`${PMS_BASEURL}/pms/categories/list-by-brand`,params);
  },

  /**
   * 根据父级类别id查询子级类别列表接口
   * @param {Object} params 查询类别所需参数对象，
   *  例如：{parentId:父类类别ID, page:页码, sizeNum:每页记录数}
   * @returns {Promise}
   */
  queryByParent(params) {
    return myAxios.get( `${PMS_BASEURL}/pms/categories/list-by-parent`,params);
  },

  /**
   * 更新类别接口
   * @param {Object} params 待更新的类别对象，详见接口文档。
   * @returns {Promise}
   */
  update(params) {
    return myAxios.post(
      `${PMS_BASEURL}/pms/categories/${params.id}/full-info/update`,
      params
    );
  },

  /**
   * 更新类别, 启用类别接口
   * @param {Object} params 包含类别ID的类别对象
   * @returns {Promise}
   */
  enable(params) {
    return myAxios.post(
      `${PMS_BASEURL}/pms/categories/${params.id}/status/enable`,
      params
    );
  },

  /**
   * 更新类别, 禁用类别接口
   * @param {Object} params 包含类别ID的类别对象
   * @returns {Promise}
   */
  disable(params) {
    return myAxios.post(
      `${PMS_BASEURL}/pms/categories/${params.id}/status/disable`,
      params
    );
  },

  /**
   * 更新类别, 显示类别接口
   * @param {Object} params 包含类别ID的类别对象
   * @returns {Promise}
   */
  show(params) {
    return myAxios.post(
      `${PMS_BASEURL}/pms/categories/${params.id}/status/show`,
      params
    );
  },

  /**
   * 更新类别, 隐藏类别接口
   * @param {Object} params 包含类别ID的类别对象
   * @returns {Promise}
   */
  hide(params) {
    return myAxios.post(
      `${PMS_BASEURL}/pms/categories/${params.id}/status/hide`,
      params
    );
  },

  /**
   * 删除类别接口
   * @param {Object} params 包含待删除的类别ID的参数对象，例如： {id: 类别ID}
   * @returns {Promise}
   */
  delete(params) {
    return myAxios.post(`${PMS_BASEURL}/pms/categories/${params.id}/delete`);
  },

  /**
   * 添加类别接口
   * @param {Object} params 待添加的类别对象，详见接口文档。
   * @returns  {Promise}
   */
  add(params) {
    return myAxios.post(`${PMS_BASEURL}/pms/categories/addnew`, params);
  },



  /**
   * 重建缓存
   * @returns  {Promise}
   */
  rebuildCache() {
    return myAxios.post(`${PMS_BASEURL}/pms/categories/rebuild-cache`);
  },
};

export default categoryApi;
