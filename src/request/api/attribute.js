/** 属性业务模块接口整合 */
import base from './baseurl'           // 导入接口请求地址的前缀字符串，例如： http://localhost:9010
import myAxios from '../MyAxios'         // 导入自定义Axios，该Axios重新封装了get与post方法，方便传参。

let PMS_BASEURL = base.PMS_BASEURL

const attributeApi = {   

     /**
     * 通过属性ID查询
     * @param {Object} params 属性对象 包含属性ID 
     *      例如： {id: 1}
     */
         queryById(params){
            return myAxios.get(`${PMS_BASEURL}/pms/attributes/${params.id}/details`, params)
        },

    /**
     * 通过属性模板ID查询
     * @param {Object} params 属性模版对象 包含属性模板ID 
     *      例如： {templateId: 1, page:x, pageSize:x}
     */
     queryByTemplateId(params){
        return myAxios.get(`${PMS_BASEURL}/pms/attributes`, params)
    },

    /**
     * 通过属性模板ID查询非销售属性列表
     * @param {Object} params 属性模版对象 包含属性模板ID 
     *      例如： {templateId: 1, page:x, pageSize:x}
     */
     queryNonSalesByTemplateId(params){
        return myAxios.get(`${PMS_BASEURL}/pms/attributes/non-sale`, params)
    },

    /**
     * 通过属性模板ID查询销售属性列表
     * @param {Object} params 属性模版对象 包含属性模板ID 
     *      例如： {templateId: 1, page:x, pageSize:x}
     */
     querySalesByTemplateId(params){
        return myAxios.get(`${PMS_BASEURL}/pms/attributes/sale`, params)
    },
    

    /**
     * 删除属性接口
     * @param {Object} params 包含待删除的属性ID的参数对象，例如： {id: 属性ID}
     * @returns {Promise}
     */
     delete (params) {
        return myAxios.post(`${PMS_BASEURL}/pms/attributes/${params.id}/delete`)
    },

    /**
     * 修改属性接口
     * @param {Object} params 待修改的属性对象，
     *      例如：{id: 1, name:"名称" ..}
     * @returns  {Promise}
     */
     update (params) {
        return myAxios.post(
            `${PMS_BASEURL}/pms/attributes/${params.id}/update`, params)
    },

    /**
     * 添加属性接口
     * @param {Object} params 待添加的属性对象，
     *      例如：{name:"名称"}
     * @returns  {Promise}
     */
    add (params) {
        return myAxios.post( `${PMS_BASEURL}/pms/attributes/addnew`,  params)
    }
}

export default attributeApi