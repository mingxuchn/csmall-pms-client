/** category业务模块接口整合 */
import base from './baseurl'           // 导入接口域名列表
import myAxios from '../MyAxios'         // 导入http中创建的axios实例


let PMS_BASEURL = base.PMS_BASEURL

const skuApi = {   
    /**
     * 新增sku
     * @param {Object} params 包含完整sku表单的对象
     * @returns {Promise}
     */
    add (params) {
        return myAxios.post(`${PMS_BASEURL}/pms/sku/addnew`, params)
    },

    /**
     * 根据skuId查询sku详情
     * @param {Object} params 参数对象
     *  例如：{id:1}
     * @returns {Promise}
     */
    queryById(params) {
        return myAxios.get(`${PMS_BASEURL}/pms/sku/${params.id}`)
    },

    /**
     * 根据spuId查询sku列表
     * @param {Object} params 参数对象
     *  例如：{spuId:1, page:1, pageSize:10}
     * @returns {Promise}
     */
     queryBySpuId(params) {
        return myAxios.get( `${PMS_BASEURL}/pms/sku`,params)
    },

    /**
     * 更新sku接口
     * @param {Object} params 待更新的sku对象，详见接口文档。
     * @returns {Promise}
     */
    update (params) {
        return myAxios.post(
            `${PMS_BASEURL}/pms/sku/${params.id}/update`, 
            params
        )
    }
    
}

export default skuApi