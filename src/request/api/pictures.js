/** category业务模块接口整合 */
import base from './baseurl'           // 导入接口域名列表
import myAxios from '../MyAxios'         // 导入http中创建的axios实例


let PMS_BASEURL = base.PMS_BASEURL

const picturesApi = {   


    /**
     * 将图片新增到相册
     * @param {Object} params 待添加的图片对象，需包含albumId，详见接口文档。
     * @returns  {Promise}
     */
    add (params) {
        return myAxios.post(
            `${PMS_BASEURL}/pms/pictures/addnew`, 
            params
        )
    },

    /**
     * 根据相册id查询图片列表
     * @param {Object} params  封装了相册id的参数对象
     *      例如：{albumId: 4}
     * @returns 
     */
    queryByAlbumId (params) {
        return myAxios.get( `${PMS_BASEURL}/pms/pictures`, params )
    },

    /**
     * 设置图片为相册封面
     * @param {Object} params  封装了图片id的参数对象
     *      例如：{id: 4}
     * @returns 
     */
     setCover (params) {
        return myAxios.get( `${PMS_BASEURL}/pms/pictures/${params.id}/set-cover`)
    },
}

export default picturesApi