/** api接口的统一出口 */
import userApi from './user.js'
import brandApi from './brand.js'
import categoryApi from './category.js'
import brandCategoryApi from './brandCategory.js'
import albumApi from './album.js'
import attrTemplateApi from './attrTemplate.js'
import attributeApi from './attribute.js'
import picturesApi from './pictures.js'
import spuApi from './spu.js'
import skuApi from './sku.js'
import brandCategoryRelationsApi from './brandCategoryRelations.js'

// 将各业务模块接口导出
export default {    
    userApi,
    brandApi,
    categoryApi,
    brandCategoryApi,
    albumApi,
    attrTemplateApi,
    attributeApi,
    picturesApi,
    spuApi,
    skuApi,
    brandCategoryRelationsApi
    // ……
}