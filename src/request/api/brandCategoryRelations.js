/** category业务模块接口整合 */
import base from './baseurl'           // 导入接口域名列表
import myAxios from '../MyAxios'         // 导入http中创建的axios实例


let PMS_BASEURL = base.PMS_BASEURL

const brandCategoryRelationsApi = {   


    /**
     * 为品牌ID与类别ID绑定关联关系接口
     * @param {Object} params 参数对象，需包含brandId与categoryId，详见接口文档。
     * @returns  {Promise}
     */
    bind (params) {
        return myAxios.post(
            `${PMS_BASEURL}/pms/brand-category-relations/bind`, 
            params
        )
    },


    /**
     * 为品牌ID与类别ID解除绑定关联关系接口
     * @param {Object} params 参数对象，需包含brandId与categoryId，详见接口文档。
     * @returns  {Promise}
     */
     unbind (params) {
        return myAxios.post(
            `${PMS_BASEURL}/pms/brand-category-relations/unbind`, 
            params
        )
    },
}

export default brandCategoryRelationsApi