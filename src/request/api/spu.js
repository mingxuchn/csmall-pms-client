/** category业务模块接口整合 */
import base from './baseurl'           // 导入接口域名列表
import myAxios from '../MyAxios'         // 导入http中创建的axios实例

let PMS_BASEURL = base.PMS_BASEURL;

const spuApi = {
  /**
   * 新增spu
   * @param {Object} params 包含完整spu表单的对象
   * @returns {Promise}
   */
  add(params) {
    return myAxios.post(`${PMS_BASEURL}/pms/spu/addnew`, params);
  },

  /**
   * 查询spu列表
   * @param {Object} params 参数对象
   *  例如：{page:1, pageSize:5}
   * @returns {Promise}
   */
  query(params) {
    return myAxios.get(`${PMS_BASEURL}/pms/spu`, params);
  },

  /**
   * 通过spuId查询spu详情
   * @param {Object} params 包含id的参数对象
   *  例如：{id:1}
   * @returns {Promise}
   */
  queryById(params) {
    return myAxios.get(`${PMS_BASEURL}/pms/spu/${params.id}`);
  },

  /**
   * 同步SPU库存（用于增减SKU或SKU的库存变化后SPU库存未更新的情况）
   * @param {Object} params 包含id的参数对象
   * @returns {Promise}
   */
  synchroniseStock(params) {
    return myAxios.post(
      `${PMS_BASEURL}/pms/spu/${params.id}/stock/synchronise`,
      params
    );
  },

  /**
   * 同步SPU价格（用于增减SKU或SKU的库存变化后SPU库存未更新的情况）
   * @param {Object} params 包含id的参数对象
   * @returns {Promise}
   */
  synchronisePrice(params) {
    return myAxios.post(
      `${PMS_BASEURL}/pms/spu/${params.id}/price/synchronise`,
      params
    );
  },
};

export default spuApi;
