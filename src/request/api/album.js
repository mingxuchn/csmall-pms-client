/** 相册业务模块接口整合 */
import base from './baseurl'           // 导入接口请求地址的前缀字符串，例如： http://localhost:9010
import myAxios from '../MyAxios'         // 导入自定义Axios，该Axios重新封装了get与post方法，方便传参。

let PMS_BASEURL = base.PMS_BASEURL

const albumsApi = {   
    /**
     * 根据ID查询相册详情接口
     * @param {Object} params 查询相册详情所需参数对象，
     *      例如：{id : 14}
     * @returns {Promise}
     */
     queryById (params) {
        return myAxios.get(`${PMS_BASEURL}/pms/albums/${params.id}`)
    },

    /**
     * 查询相册接口
     * @param {Object} params 查询相册所需参数对象，
     *      例如：{page, pageSize}
     * @returns {Promise}
     */
    query (params) {
        return myAxios.get(`${PMS_BASEURL}/pms/albums`, params)
    },

    /**
     * 更新相册接口
     * @param {Object} params 待修改的相册对象，详见接口文档。
     * @returns {Promise}
     */
    update (params) {
        return myAxios.post( `${PMS_BASEURL}/pms/albums/${params.id}/update`, params)
    },

    /**
     * 删除相册接口
     * @param {Object} params 包含待删除的品牌ID的参数对象，例如： {id: 品牌ID}
     * @returns {Promise}
     */
    delete (params) {
        return myAxios.post(`${PMS_BASEURL}/pms/brands/${params.id}/delete`)
    },

    /**
     * 添加相册接口
     * @param {Object} params 待添加的相册对象，
     *      例如：{description:"描述", name:"商品名称", sort：自定义排序序号}
     * @returns  {Promise}
     */
    add (params) {
        return myAxios.post(
            `${PMS_BASEURL}/pms/albums/addnew`, params)
    }
}

export default albumsApi