/** brand业务模块接口整合 */
import base from './baseurl'           // 导入接口域名列表
import myAxios from '../MyAxios'         // 导入http中创建的axios实例


let PMS_BASEURL = base.PMS_BASEURL

const brandApi = {   

    /**
     * 根据品牌ID， 查询品牌详情接口
     * @param {number} id 品牌ID
     */
    queryById(id){
        return myAxios.get(`${PMS_BASEURL}/pms/brands/${id}`)
    },

    /**
     * 查询品牌列表接口
     * @param {Object} params 查询品牌所需参数对象，
     *  例如：{page:页码, pageSize:每页记录数} 
     * 
     * @returns {Promise}
     */
    query (params) {
        return myAxios.get(`${PMS_BASEURL}/pms/brands`, params)
    },

    /**
     * 根据类别id查询品牌列表接口
     * @param {Object} params 查询品牌所需参数对象，
     *        例如：{categoryId:类别ID}
     * @returns {Promise}
     */
    queryByCategoryId(params) {
        return myAxios.get( `${PMS_BASEURL}/pms/brands/by-category`,params);
    },
    
    /**
     * 修改品牌接口
     * @param {Object} params 待修改的品牌对象，详见接口文档。
     * @returns {Promise}
     */
    update (params) {
        return myAxios.post(
            `${PMS_BASEURL}/pms/brands/${params.id}/update`, 
            params
        )
    },

    /**
     * 删除品牌接口
     * @param {Object} params 包含待删除的品牌ID的参数对象，例如： {id: 品牌ID}
     * @returns {Promise}
     */
    delete (params) {
        return myAxios.post(`${PMS_BASEURL}/pms/brands/${params.id}/delete`)
    },

    /**
     * 添加品牌接口
     * @param {Object} params 待添加的品牌对象，详见接口文档。
     * @returns  {Promise}
     */
    add (params) {
        return myAxios.post( `${PMS_BASEURL}/pms/brands/addnew`,  params
        )
    },

  /**
   * 更新品牌, 启用品牌接口
   * @param {Object} params 包含品牌ID的品牌对象
   * @returns {Promise}
   */
   enable(params) {
    return myAxios.post( `${PMS_BASEURL}/pms/brands/${params.id}/status/enable`, params );
  },

  /**
   * 更新品牌, 禁用品牌接口
   * @param {Object} params 包含品牌ID的品牌对象
   * @returns {Promise}
   */
  disable(params) {
    return myAxios.post( `${PMS_BASEURL}/pms/brands/${params.id}/status/disable`, params );
  },

    
    /**
     * 重建缓存
     * @returns  {Promise}
     */
    rebuildCache(){
        return myAxios.post(
            `${PMS_BASEURL}/pms/brands/rebuild-cache`
        )
    }
}

export default brandApi