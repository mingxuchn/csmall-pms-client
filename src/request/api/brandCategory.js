/** 类别与品牌管理业务模块接口整合 */
import base from './baseurl'           // 导入接口域名列表
import myAxios from '../MyAxios'         // 导入http中创建的axios实例


let PMS_BASEURL = base.PMS_BASEURL

const brandCategoryApi = {   

    /**
     * 解绑类别和品牌接口
     * @param {Object} params 包含类别ID与品牌ID的参数对象，例如： {brandId:1, categoryId:2}
     * @returns {Promise}
     */
    unbind (params) {
        return myAxios.post(
            `${PMS_BASEURL}/pms/brand-category-relations/unbind`, 
            params
        )
    },

    /**
     * 绑定类别和品牌接口
     * @param {Object} params 包含类别ID与品牌ID的参数对象，例如： {brandId:1, categoryId:2}
     * @returns {Promise}
     */
    bind (params) {
        return myAxios.post(
            `${PMS_BASEURL}/pms/brand-category-relations/bind`, 
            params
        )
    }
}

export default brandCategoryApi