/** 属性模板业务模块接口整合 */
import base from './baseurl'           // 导入接口域名列表
import myAxios from '../MyAxios'         // 导入http中创建的axios实例

let PMS_BASEURL = base.PMS_BASEURL

const attrTemplateApi = {   

    /**
     * 根据类别id查询属性模版列表
     * @param {Object} params  封装了类别ID的参数对象
     *      例如：{categoryId: 8}
     * @returns 
     */
    queryByCategoryId (params) {
        return myAxios.get( `${PMS_BASEURL}/pms/attribute-templates/list-by-category`, params )
    },

    /**
     * 根据模版ID查询详情接口（含包含的所有子属性）
     * @param {Object} params  封装了模版ID的参数对象
     *      例如：{id: 8}
     * @returns 
     */
    queryById(params) {
        return myAxios.get(`${PMS_BASEURL}/pms/attribute-templates/${params.id}/details`)
    },

    /**
     * 查询所有属性模板接口
     * @param {Object} params 查询所有属性模板所需参数对象，
     *      例如：{page, pageSize}
     * @returns {Promise}
     */
    queryAll (params) {
        return myAxios.get(`${PMS_BASEURL}/pms/attribute-templates`, params)
    },

    /**
     * 更新属性模板接口
     * @param {Object} params 待修改的属性模板对象，详见接口文档。
     * @returns {Promise}
     */
    update (params) {
        return myAxios.post( `${PMS_BASEURL}/pms/attribute-templates/${params.id}/update`,  params)
    },

    /**
     * 删除属性模板接口
     * @param {Object} params 包含待删除的属性模板ID的参数对象，例如： {id: 属性模板ID}
     * @returns {Promise}
     */
    delete (params) {
        return myAxios.post(`${PMS_BASEURL}/pms/attribute-templates/${params.id}/delete`)
    },

    /**
     * 添加属性模板接口
     * @param {Object} params 待添加的属性模板对象，
     *      例如：{name:"模板名称"}
     * @returns  {Promise}
     */
    add (params) {
        return myAxios.post(
            `${PMS_BASEURL}/pms/attribute-templates/addnew`, 
            params)
    }
}

export default attrTemplateApi