const EXPORT_STORAGE = {

    KEY_ADD_SPU: 'KEY_ADD_SPU',
    KEY_USERINFO: 'KEY_USERINFO',
    KEY_TOKENINFO: 'KEY_TOKENINFO',

    /** 向localStorage中存储 */
    save : function (key, value){
        localStorage.setItem(`${key}`, JSON.stringify(value))
    },

    /** 从localStorage中读取 */
    get : function (key){
        return JSON.parse(localStorage.getItem(`${key}`))
    }, 

    /** 删除 */
    remove : function (key){
        return localStorage.removeItem(`${key}`)
    }

}

export default EXPORT_STORAGE;
 
 