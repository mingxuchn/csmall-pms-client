import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    redirect: '/home/index'
  },
  {
    path: '/home',
    name: '/home',
    component: Home,
    redirect: '/home/index',
    children: [
      {
        path: 'index',
        name: '/home/index',
        component: () => import('../views/index/Index.vue')
      },
      {
        path: 'product/spumanage',
        name: 'product/spumanage',
        component: () => import('../views/product/spu/SpuManage.vue'),
        meta: {
          routerPath: 'spumanage'
        }
      },

      {
        path: 'product/skumanage/:id',
        name: 'product/skumanage',
        component: () => import('../views/product/sku/SkuManage.vue'),
        meta: {
          routerPath: 'spumanage'
        }
      },
      {
        path: 'product/addspu/1',
        name: 'product/addspu/1',
        component: () => import('../views/product/spu/SpuAddStep1.vue'),
        meta: {
          routerPath: 'addspu'
        }
      },
      {
        path: 'product/addspu/2',
        name: 'product/addspu/2',
        component: () => import('../views/product/spu/SpuAddStep2.vue'),
        meta: {
          routerPath: 'addspu'
        }
      },
      {
        path: 'product/addspu/3',
        name: 'product/addspu/3',
        component: () => import('../views/product/spu/SpuAddStep3.vue'),
        meta: {
          routerPath: 'addspu'
        }
      },
      {
        path: 'product/addspu/4',
        name: 'product/addspu/4',
        component: () => import('../views/product/spu/SpuAddStep4.vue'),
        meta: {
          routerPath: 'addspu'
        }
      },
    
      {
        path: 'product/albummanage',
        name: 'product/albummanage',
        component: () => import('../views/product/album/AlbumManage.vue'),
        meta: {
          routerPath: 'albummanage'
        }
      },
      {
        path: 'product/albummanage/:albumId',
        name: 'product/albummanage/albumId',
        component: () => import('../views/product/album/PictureManage.vue'),
        meta: {
          routerPath: 'albummanage'
        }
      },
      {
        path: 'product/category',
        name: 'product/category',
        component: () => import('../views/product/category/CategoryManage.vue'),
        meta: {
          routerPath: 'category'
        }
      },
      {
        path: 'product/addcategory',
        name: 'product/addcategory',
        component: () => import('../views/product/category/CategoryAdd.vue'),
        meta: {
          routerPath: 'category'
        }
      },
      {
        path: 'product/updatecategory',
        name: 'product/updatecategory',
        component: () => import('../views/product/category/CategoryUpdate.vue'),
        meta: {
          routerPath: 'category'
        }
      },
      {
        path: 'product/attributetemplatemanage',
        name: 'product/attributetemplatemanage',
        component: () => import('../views/product/attribute/AttributeTemplateManage.vue'),
        meta: {
          routerPath: 'attributetemplate'
        }
      },
      {
        path: 'product/nosalesattributemanage/:attributeTemplateId',
        name: 'product/nosalesattributemanage',
        component: () => import('../views/product/attribute/NoSalesAttributeManage.vue'),
        meta: {
          routerPath: 'attributetemplate'
        }
      },
      {
        path: 'product/addnosalesattribute/:attributeTemplateId',
        name: 'product/addnosalesattribute',
        component: () => import('../views/product/attribute/NoSalesAttributeAdd.vue'),
        meta: {
          routerPath: 'attributetemplate'
        }
      },
      {
        path: 'product/updatenosalesattribute/:attributeId',
        name: 'product/updatenosalesattribute',
        component: () => import('../views/product/attribute/NoSalesAttributeUpdate.vue'),
        meta: {
          routerPath: 'attributetemplate'
        }
      },
      {
        path: 'product/addsalesattribute/:attributeTemplateId',
        name: 'product/addsalesattribute',
        component: () => import('../views/product/attribute/SalesAttributeAdd.vue'),
        meta: {
          routerPath: 'attributetemplate'
        }
      },
      {
        path: 'product/updatesalesattribute/:attributeId',
        name: 'product/updatesalesattribute',
        component: () => import('../views/product/attribute/SalesAttributeUpdate.vue'),
        meta: {
          routerPath: 'attributetemplate'
        }
      },
      {
        path: 'product/salesattributemanage/:attributeTemplateId',
        name: 'product/salesattributemanage',
        component: () => import('../views/product/attribute/SalesAttributeManage.vue'),
        meta: {
          routerPath: 'attributetemplate'
        }
      },
      {
        path: 'product/brandmanage',
        name: 'product/brandmanage',
        component: () => import('../views/product/brand/BrandManage.vue'),
        meta: {
          routerPath: 'brandmanage'
        }
      },
      {
        path: 'product/addbrand',
        name: 'product/addbrand',
        component: () => import('../views/product/brand/BrandAdd.vue'),
        meta: {
          routerPath: 'brandmanage'
        }
      },
      {
        path: 'product/updatebrand/:id',
        name: 'product/updatebrand',
        component: () => import('../views/product/brand/BrandUpdate.vue'),
        meta: {
          routerPath: 'brandmanage'
        }
      },
      {
        path: 'auth/userlist',
        name: 'auth/userlist',
        component: () => import('../views/auth/UserList.vue'),
        meta: {
          routerPath: 'userlist'
        }
      },
      {
        path: 'auth/rolelist',
        name: 'auth/rolelist',
        component: () => import('../views/auth/RoleList.vue'),
        meta: {
          routerPath: 'rolelist'
        }
      },
      {
        path: 'auth/permissionlist',
        name: 'auth/permissionlist',
        component: () => import('../views/auth/PermissionList.vue'),
        meta: {
          routerPath: 'permissionlist'
        }
      },
      {
        path: 'auth/permissionassignment',
        name: 'auth/permissionassignment',
        component: () => import('../views/auth/PermissionAssignment.vue'),
        meta: {
          routerPath: 'permissionassignment'
        }
      }
    ] 
  },
  {
    path: '/user/login',
    name: '/user/login',
    component: () => import('../views/user/Login.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
