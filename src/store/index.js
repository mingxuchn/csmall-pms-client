import Vue from 'vue'
import Vuex from 'vuex'
import StorageUtils from '@/common/StorageUtils'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    userInfo : StorageUtils.get('userInfo'),
    tokenInfo: StorageUtils.get('tokenInfo')
  },
  mutations: {

    /** 退出登录 */
    logout(state){
      state.userInfo = {}
      state.tokenInfo = {}
      // 清除用户信息的本地缓存
      StorageUtils.remove(StorageUtils.KEY_USERINFO)
      StorageUtils.remove(StorageUtils.KEY_TOKENINFO)
    },

    /** 处理登录成功后的数据存储 */
    loginSuccess(state, userInfo){
      state.userInfo = userInfo
      // 缓存 刷新不失效
      StorageUtils.save(StorageUtils.KEY_USERINFO, userInfo)
    }, 

    /** 处理Token的保存 */
    saveToken(state, tokenInfo){
      state.tokenInfo = tokenInfo
      StorageUtils.save(StorageUtils.KEY_TOKENINFO, tokenInfo)
    }

  },
  actions: {
  },
  modules: {
  }
})
