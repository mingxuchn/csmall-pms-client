import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

/** 引入ElementUI */
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
Vue.use(ElementUI)

// 引入WangEditor富文本编辑器
import WEditor from 'wangeditor'
Vue.prototype.WEditor = WEditor

// 引入自定义api接口  供客户端发送ajax请求
import userApi from './request/api/user.js'
import brandApi from './request/api/brand.js'
import categoryApi from './request/api/category.js'
import brandCategoryApi from './request/api/brandCategory.js'
import albumApi from './request/api/album.js'
import attrTemplateApi from './request/api/attrTemplate.js'
import attributeApi from './request/api/attribute.js'
import picturesApi from './request/api/pictures.js'
import spuApi from './request/api/spu.js'
import skuApi from './request/api/sku.js'
import brandCategoryRelationsApi from './request/api/brandCategoryRelations.js'

Vue.prototype.userApi = userApi
Vue.prototype.brandApi = brandApi
Vue.prototype.categoryApi = categoryApi
Vue.prototype.brandCategoryApi = brandCategoryApi
Vue.prototype.albumApi = albumApi
Vue.prototype.attrTemplateApi = attrTemplateApi
Vue.prototype.attributeApi = attributeApi
Vue.prototype.picturesApi = picturesApi
Vue.prototype.spuApi = spuApi
Vue.prototype.skuApi = skuApi
Vue.prototype.brandCategoryRelationsApi = brandCategoryRelationsApi

// 引入BASEURL
import BASEURL from './request/api/baseurl'
Vue.prototype.BASEURL = BASEURL

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
